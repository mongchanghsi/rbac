## RBAC Back-end Local Set-up

1. Create a database with the credentials of

```
DATABASE_NAME = rbac
DATABASE_USER = postgres
DATABASE_PASSWORD = password
```

2. Clone repo
3. Run `npm run start`
