require('dotenv').config();
export const databaseConfig = {
  name: process.env.DB_NAME || 'rbac',
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT ? +process.env.DB_PORT : 5432,
  username: process.env.DB_USERNAME || '',
  password: process.env.DB_PASSWORD || '',
};
