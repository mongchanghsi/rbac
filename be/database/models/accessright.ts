// This table has roles - (resources - permissions)
import { Model, DataTypes } from 'sequelize';
import db from '../../config/database';
import ResourcePermission from './resourcepermission';
import Role from './role';

interface AccessRightAttribute {
  id: string;
  resourcepermissionId: string;
  roleId: string;
}

export interface AccessRightInstance
  extends Model<AccessRightAttribute>,
    AccessRightAttribute {}

const AccessRight = db.define<AccessRightInstance>('accessright', {
  id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
  },
  resourcepermissionId: {
    type: DataTypes.STRING,
    references: {
      model: ResourcePermission,
      key: 'id',
    },
  },
  roleId: {
    type: DataTypes.STRING,
    references: {
      model: Role,
      key: 'id',
    },
  },
});

export default AccessRight;
