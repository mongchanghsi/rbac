// This table has users - roles - projects
import { Model, DataTypes } from 'sequelize';
import db from '../../config/database';
import Project from './project';
import Role from './role';
import User from './user';

interface AppointmentAttribute {
  id: string;
  userId: string;
  roleId: string;
  projectId?: string;
}

export interface AppointmentInstance
  extends Model<AppointmentAttribute>,
    AppointmentAttribute {}

const Appointment = db.define<AppointmentInstance>('appointment', {
  id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
  },
  userId: {
    type: DataTypes.STRING,
    references: {
      model: User,
      key: 'id',
    },
  },
  roleId: {
    type: DataTypes.STRING,
    references: {
      model: Role,
      key: 'id',
    },
  },
  projectId: {
    type: DataTypes.STRING,
    references: {
      model: Project,
      key: 'id',
    },
  },
});

export default Appointment;
