import { Model, DataTypes } from 'sequelize';
import db from '../../config/database';

interface PermissionAttributes {
  id: string;
  permission_name: string;
  desc: string;
}

export interface PermissionInstance
  extends Model<PermissionAttributes>,
    PermissionAttributes {}

const Permission = db.define<PermissionInstance>('permission', {
  id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
  },
  permission_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  desc: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

export default Permission;
