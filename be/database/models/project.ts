import {
  Model,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyRemoveAssociationMixin,
} from 'sequelize';
import db from '../../config/database';
import { ResourceInstance } from './resource';

interface ProjectAttributes {
  id: string;
  title: string;
  desc: string;
}

export interface ProjectInstance
  extends Model<ProjectAttributes>,
    ProjectAttributes {
  addResource: HasManyAddAssociationMixin<ResourceInstance, string>;
  removeResource: HasManyRemoveAssociationMixin<ResourceInstance, string>;
}

const Project = db.define<ProjectInstance>('project', {
  id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  desc: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

export default Project;
