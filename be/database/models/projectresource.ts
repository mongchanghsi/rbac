import { Model, DataTypes } from 'sequelize';
import db from '../../config/database';
import Resource from './resource';
import Project from './project';

interface ProjectResourceAttributes {
  resourceId: string;
  projectId: string;
}

export interface ProjectResourceInstance
  extends Model<ProjectResourceAttributes>,
    ProjectResourceAttributes {}

const ProjectResource = db.define<ProjectResourceInstance>('projectresource', {
  resourceId: {
    type: DataTypes.STRING,
    references: {
      model: Resource,
      key: 'id',
    },
  },
  projectId: {
    type: DataTypes.STRING,
    references: {
      model: Project,
      key: 'id',
    },
  },
});

export default ProjectResource;
