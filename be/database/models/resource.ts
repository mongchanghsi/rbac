import {
  Model,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyRemoveAssociationMixin,
} from 'sequelize';
import db from '../../config/database';
import { ProjectInstance } from './project';

interface ResourceAttributes {
  id: string;
  resource_name: string;
  type: string;
}

export interface ResourceInstance
  extends Model<ResourceAttributes>,
    ResourceAttributes {
  addProject: HasManyAddAssociationMixin<ProjectInstance, string>;
  removeProject: HasManyRemoveAssociationMixin<ProjectInstance, string>;
}

const Resource = db.define<ResourceInstance>('resource', {
  id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
  },
  resource_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  type: {
    type: DataTypes.STRING,
    allowNull: true,
  },
});

export default Resource;
