import {
  Model,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyRemoveAssociationMixin,
} from 'sequelize';
import db from '../../config/database';
import Resource from './resource';
import Permission from './permission';
import { RoleInstance } from './role';

interface ResourcePermissionAttributes {
  id: string;
  name: string;
  resourceId: string;
  permissionId: string;
  desc: string;
}

export interface ResourcePermissionInstance
  extends Model<ResourcePermissionAttributes>,
    ResourcePermissionAttributes {
  addRole: HasManyAddAssociationMixin<RoleInstance, string>;
  removeRole: HasManyRemoveAssociationMixin<RoleInstance, string>;
}

const ResourcePermission = db.define<ResourcePermissionInstance>(
  'resourcepermission',
  {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    resourceId: {
      type: DataTypes.STRING,
      references: {
        model: Resource,
        key: 'id',
      },
    },
    permissionId: {
      type: DataTypes.STRING,
      references: {
        model: Permission,
        key: 'id',
      },
    },
    desc: {
      type: DataTypes.STRING,
    },
  },
);

export default ResourcePermission;
