import {
  Model,
  DataTypes,
  HasManyAddAssociationMixin,
  HasManyRemoveAssociationMixin,
} from 'sequelize';
import db from '../../config/database';
import { ResourcePermissionInstance } from './resourcepermission';

interface RoleAttributes {
  id: string;
  role_name: string;
}

export interface RoleInstance extends Model<RoleAttributes>, RoleAttributes {
  addResourcePermission: HasManyAddAssociationMixin<
    ResourcePermissionInstance,
    string
  >;
  removeResourcePermission: HasManyRemoveAssociationMixin<
    ResourcePermissionInstance,
    string
  >;
}

const Role = db.define<RoleInstance>('role', {
  id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
  },
  role_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

export default Role;
