import {
  HasManyAddAssociationMixin,
  HasManyCountAssociationsMixin,
  Model,
  DataTypes,
  HasManyRemoveAssociationMixin,
} from 'sequelize';
import db from '../../config/database';
import { RoleInstance } from './role';

interface UserAttributes {
  id: string;
  username: string;
  password: string;
  token?: string;
}

export interface UserInstance extends Model<UserAttributes>, UserAttributes {}

const User = db.define<UserInstance>('user', {
  id: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  token: {
    type: DataTypes.STRING,
    allowNull: true,
  },
});

export default User;
