import express, { Express } from 'express';
import db from './config/database';
import cors from 'cors';
import cookieParser from 'cookie-parser';

// Import Models
import User from './database/models/user';
import Role from './database/models/role';
import Permission from './database/models/permission';
import Appointment from './database/models/appointment';
import Project from './database/models/project';
import Resource from './database/models/resource';
import ResourcePermission from './database/models/resourcepermission';
import AccessRight from './database/models/accessright';
import ProjectResource from './database/models/projectresource';

// Import Routes
import UserAPI from './routes/user';
import RoleAPI from './routes/role';
import PermissionAPI from './routes/permission';
import AuthAPI from './routes/auth';
import ProjectAPI from './routes/project';
import AppointmentAPI from './routes/appointment';
import ResourceAPI from './routes/resource';
import ResourcePermissionAPI from './routes/resourcepermission';
import AccessRightAPI from './routes/accessright';

// Express Set up
const app: Express = express();
app.use(express.json());
app.use(cors({ origin: 'http://localhost:3000', credentials: true }));
app.use(cookieParser());

// Postgres Set up
db.authenticate()
  .then(() => console.log('Connected to DB'))
  .catch((error) => console.log(error.message));

// API routes
app.use('/api/user', UserAPI);
app.use('/api/role', RoleAPI);
app.use('/api/permission', PermissionAPI);
app.use('/api/auth', AuthAPI);
app.use('/api/project', ProjectAPI);
app.use('/api/appointment', AppointmentAPI);
app.use('/api/resource', ResourceAPI);
app.use('/api/resourcepermission', ResourcePermissionAPI);
app.use('/api/accessright', AccessRightAPI);

// Associations
Resource.belongsToMany(Permission, { through: ResourcePermission });
Permission.belongsToMany(Resource, { through: ResourcePermission });

// One project has many resource
Project.belongsToMany(Resource, { through: ProjectResource });
Resource.belongsToMany(Project, { through: ProjectResource });

// Role has many Resource-Permission
Role.belongsToMany(ResourcePermission, { through: AccessRight });
ResourcePermission.belongsToMany(Role, { through: AccessRight });

// Sync
User.sync();
Role.sync();
Permission.sync();
Appointment.sync();
Project.sync();
Resource.sync();
ResourcePermission.sync();
AccessRight.sync();
ProjectResource.sync();

// Start-up
const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening to port ${port}...`));
