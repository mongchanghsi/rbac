import express, { Request, Response } from 'express';
import AccessRight from '../database/models/accessright';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router({
  strict: true,
});

// @route   GET /
// @desc    Retrive all access rights
// @access  Private
router.get(
  '/',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const accessright = await AccessRight.findAll({
        include: [{ all: true }],
      });
      if (accessright.length > 0) {
        res.json(accessright);
      } else {
        res.status(404).json({ message: 'No Access Rights found' });
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /create
// @desc    Create a new access right
// @access  Private
router.post(
  '/create',
  async (req: Request, res: Response): Promise<void> => {
    const id = uuidv4();
    const roleId = req.body.roleId;
    const resourcepermissionId = req.body.resourcepermissionId;

    try {
      // Does access right exist
      const accessright = await AccessRight.findOne({
        where: { roleId, resourcepermissionId },
      });
      if (accessright) {
        res.status(400).json({ message: 'Role already exist' });
      } else {
        // If access right does not exist
        let newAccessRight = await AccessRight.create({
          id,
          roleId,
          resourcepermissionId,
        });
        res.json(newAccessRight);
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   delete /remove
// @desc    Remove an access right from a role
// @access  Private
router.delete(
  '/remove',
  async (req: Request, res: Response): Promise<void> => {
    const roleId = req.body.roleId;
    const resourcepermissionId = req.body.resourcepermissionId;

    try {
      // Does access right exist
      const accessright = await AccessRight.findOne({
        where: { roleId, resourcepermissionId },
      });
      if (accessright) {
        await accessright.destroy();
        res.json({ message: 'Access Right successfully removed' });
      } else {
        res.status(404).json({ message: 'No Acccess Right Found' });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

export default router;
