import express, { Request, Response } from 'express';
import User from '../database/models/user';
import { checkPermissions, checkToken } from '../src/authentication';
import Appointment from '../database/models/appointment';
import Role from '../database/models/role';
import Project from '../database/models/project';
import { Op } from 'sequelize';

const router = express.Router();

interface RequestUser extends Request {
  user?: any;
}

// @route   GET /
// @desc    Retrieve all appointment
// @access  Public
router.get(
  '/',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const appointments = await Appointment.findAll({
        include: [{ all: true }],
      });
      if (appointments.length > 0) {
        res.json(appointments);
      } else {
        res.status(404).json({ message: 'No Appointments found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   GET /retrieve_default_role/:id
// @desc    Retrieve user's default role
// @access  Public
router.get(
  '/retrieve_default_role/:id',
  [checkToken],
  async (req: RequestUser, res: Response): Promise<void> => {
    try {
      const appointment = await Appointment.findOne({
        where: { userId: req.user.id, projectId: 'SUPER_PROJECT' },
        include: [{ all: true }],
      });
      if (appointment) {
        res.json(appointment);
      } else {
        res.status(404).json({ message: 'No Appointments found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   GET /retrieve_role/:id
// @desc    Retrieve user's role in a particular project
// @access  Public
router.get(
  '/retrieve_role/:projectId',
  [checkToken],
  async (req: RequestUser, res: Response): Promise<void> => {
    try {
      const appointment = await Appointment.findOne({
        where: { userId: req.user.id, projectId: req.params.projectId },
        include: [{ all: true }],
      });
      if (appointment) {
        res.json(appointment);
      } else {
        res.status(404).json({ message: 'No Appointments found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   GET /:id
// @desc    Retrieve appointment by userId
// @access  Public
router.get(
  '/:id',
  [checkToken],
  async (req: RequestUser, res: Response): Promise<void> => {
    try {
      // Exclude appointment entry that has a null value
      const appointment = await Appointment.findAll({
        where: {
          userId: req.user.id,
          projectId: { [Op.not]: 'SUPER_PROJECT' },
        },
        include: [{ all: true }],
      });
      if (appointment) {
        res.json(appointment);
      } else {
        res.status(404).json({ message: 'No Appointments found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /createnewuser
// @desc    Register a new appointment where the user is first registered
// @access  Private
router.post(
  '/createnewuser',
  async (req: Request, res: Response): Promise<void> => {
    const id = req.body.id;
    const userId = req.body.userId;
    const roleId = req.body.roleId;

    try {
      // Include guard condition to check whether are all Id available in the DB
      const user = await User.findOne({ where: { id: userId } });
      const role = await Role.findOne({ where: { id: roleId } });
      if (!user || !role) {
        // User or role or project does not exist
        res.status(400).json({ message: 'Invalid information provided' });
      }

      // Does appointment exist
      const appointment = await Appointment.findOne({ where: { id } });
      if (appointment) {
        res.status(400).json({ message: 'Appointment already exist' });
      } else {
        // If appointment does not exist
        let newAppointment = await Appointment.create({
          id,
          userId,
          roleId,
        });
        res.json(newAppointment);
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /access_create
// @desc    Register a new appointment
// @access  Private
router.post(
  '/access_create',
  [checkToken, checkPermissions],
  async (req: Request, res: Response): Promise<void> => {
    const id = req.body.id;
    const userId = req.body.userId;
    const roleId = req.body.roleId;
    const projectId = req.body.projectId;

    try {
      // Include guard condition to check whether are all Id available in the DB
      const user = await User.findOne({ where: { id: userId } });
      const role = await Role.findOne({ where: { id: roleId } });
      const project = await Project.findOne({ where: { id: projectId } });
      if (!user || !role || !project) {
        // User or role or project does not exist
        res.status(400).json({ message: 'Invalid information provided' });
      }

      // Does appointment exist
      const appointment = await Appointment.findOne({
        where: { userId, roleId, projectId },
      });
      if (appointment) {
        res.status(400).json({ message: 'Appointment already exist' });
      } else {
        // If appointment does not exist
        let newAppointment = await Appointment.create({
          id,
          userId,
          roleId,
          projectId,
        });
        res.json(newAppointment);
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   DELETE /access_delete
// @desc    Register a new appointment
// @access  Private
router.delete(
  '/access_delete',
  [checkToken, checkPermissions],
  async (req: Request, res: Response): Promise<void> => {
    const userId = req.body.userId;
    const roleId = req.body.roleId;
    const projectId = req.body.projectId;

    try {
      // Does appointment exist
      const appointment = await Appointment.findOne({
        where: { userId, roleId, projectId },
      });
      if (appointment) {
        await appointment.destroy();
        res.json({ message: 'Deleted appointment' });
      } else {
        res.status(404).json({ message: 'No appointment found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

export default router;
