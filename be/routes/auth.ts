import express, { Request, Response } from 'express';
import User from '../database/models/user';
import { checkPermissions, checkToken } from '../src/authentication';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router({
  strict: true,
});

interface RequestUser extends Request {
  user?: any;
}

// @route   POST /login
// @desc    Login a user and retrieve token
// @access  Private
router.post(
  '/login',
  async (req: Request, res: Response): Promise<any> => {
    // let token = 'someRandomlyGeneratedToken12345';
    let token = uuidv4();
    const username = req.body.username;
    const password = req.body.password;

    try {
      const user = await User.findOne({
        where: { username },
        include: [{ all: true }],
      });
      // Assume that uses bcrypt to decode the password
      if (user) {
        if (user.password !== password) {
          res.status(400).json({ message: 'Wrong password' });
        } else {
          user.update({ token });
          res.cookie('token', token, { httpOnly: true });
          // Redirect
          res.json(user);
        }
      } else {
        res.status(404).json({ message: 'No User of such username exist' });
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   GET /project_create
// @desc    Conduct some action
// @access  Private
router.get(
  '/project_create',
  [checkToken, checkPermissions],
  async (req: RequestUser, res: Response): Promise<any> => {
    res.send('Test Auth: Project Create Ok');
  },
);

// @route   GET /project_update
// @desc    Conduct some action
// @access  Private
router.get(
  '/project_update',
  [checkToken, checkPermissions],
  async (req: RequestUser, res: Response): Promise<any> => {
    res.send('Test Auth: Project Update Ok');
  },
);

export default router;
