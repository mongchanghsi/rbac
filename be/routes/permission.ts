import express, { Request, Response } from 'express';
import Permission from '../database/models/permission';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router({
  strict: true,
});

// @route   GET /
// @desc    Retrive all permissions
// @access  Private
router.get(
  '/',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const permissions = await Permission.findAll({
        include: [{ all: true }],
      });
      if (permissions.length > 0) {
        res.json(permissions);
      } else {
        res.status(404).json({ message: 'No Permissions found' });
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /create
// @desc    Create a new permission
// @access  Private
router.post(
  '/create',
  async (req: Request, res: Response): Promise<void> => {
    const id = uuidv4();
    const permission_name = req.body.permission_name;
    const desc = req.body.desc;

    try {
      // Does permission exist
      const permission = await Permission.findOne({
        where: { permission_name },
      });
      if (permission) {
        res.status(400).json({ message: 'Permission already exist' });
      } else {
        // If permission does not exist
        let newPermission = await Permission.create({
          id,
          permission_name,
          desc,
        });
        res.json(newPermission);
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

export default router;
