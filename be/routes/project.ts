import express, { Request, Response } from 'express';
import User from '../database/models/user';
import { checkToken, checkPermissions } from '../src/authentication';
import Project from '../database/models/project';
import Appointment from '../database/models/appointment';
import Role from '../database/models/role';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router();

interface RequestUser extends Request {
  user?: any;
}

// @route   GET /
// @desc    Retrieve all projects
// @access  Public
router.get(
  '/',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const projects = await Project.findAll({ include: [{ all: true }] });
      if (projects.length > 0) {
        res.json(projects);
      } else {
        res.status(404).json({ message: 'No Projects found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   GET /search?id=id
// @desc    Retrieve project by projectId
// @access  Public
router.get(
  '/search?',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const project = await Project.findOne({
        where: { id: req.query.id },
        include: [{ all: true }],
      });
      if (project) {
        res.json(project);
      } else {
        res.status(404).json({ message: 'No Projects found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /create
// @desc    Create a new project (without assigning user as owner)
// @access  Public
router.post(
  '/create',
  async (req: Request, res: Response): Promise<void> => {
    const id = uuidv4();
    const title = req.body.title;
    const desc = req.body.desc;

    try {
      // Does project exist
      const project = await Project.findOne({ where: { id } });
      if (project) {
        res.status(400).json({ message: 'Project already exist' });
      } else {
        // If user does not exist
        const newProject = await Project.create({ id, title, desc });
        res.json(newProject);
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /project_create
// @desc    Create a new project & assign a new appointment for the project creator
// @access  Public | Technically anyone can create a new project
router.post(
  '/project_create',
  [checkToken],
  async (req: RequestUser, res: Response): Promise<void> => {
    const projectId = uuidv4();
    const title = req.body.title;
    const desc = req.body.desc;

    try {
      // Does project exist
      const project = await Project.findOne({ where: { title } });
      if (project) {
        res.status(400).json({ message: 'Project already exist' });
      } else {
        // Create a new project
        const newProject = await Project.create({ id: projectId, title, desc });

        // Query for the Owner's ID from the database
        const role = await Role.findOne({ where: { role_name: 'Owner' } });
        if (!role) {
          res.status(404).json({ message: 'Unable to find role' });
        } else {
          // Creat a new appointment
          const newAppointment = await Appointment.create({
            id: uuidv4(),
            userId: req.user.id,
            roleId: role.id, // roleId for Owner || create a query to look for roleId based on role_name
            projectId,
          });
        }

        res.json(newProject);
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

export default router;
