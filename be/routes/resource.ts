import express, { Request, Response } from 'express';
import { checkToken } from '../src/authentication';
import Resource from '../database/models/resource';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router();

// @route   GET /
// @desc    Retrieve all resource
// @access  Public
router.get(
  '/',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const resources = await Resource.findAll({ include: [{ all: true }] });
      if (resources.length > 0) {
        res.json(resources);
      } else {
        res.status(404).json({ message: 'No Resources found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /create
// @desc    Create a new resource
// @access  Private
router.post(
  '/create',
  async (req: Request, res: Response): Promise<void> => {
    const id = uuidv4();
    const resource_name = req.body.resource_name;
    const type = req.body.type;

    try {
      // Does resource exist
      const resource = await Resource.findOne({ where: { resource_name } });
      if (resource) {
        res.status(400).json({ message: 'Resource already exist' });
      } else {
        // If user does not exist
        let newResource = await Resource.create({ id, resource_name, type });
        res.json(newResource);
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

export default router;
