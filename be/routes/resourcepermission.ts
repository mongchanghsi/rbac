import express, { Request, Response } from 'express';
import { checkToken } from '../src/authentication';
import ResourcePermission from '../database/models/resourcepermission';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router();

// @route   GET /
// @desc    Retrieve all resource
// @access  Public
router.get(
  '/',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const rps = await ResourcePermission.findAll({
        include: [{ all: true }],
      });
      if (rps.length > 0) {
        res.json(rps);
      } else {
        res.status(404).json({ message: 'No Resource-Permission found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /create
// @desc    Create a new resource-permission
// @access  Private
router.post(
  '/create',
  async (req: Request, res: Response): Promise<void> => {
    const id = uuidv4();
    const name = req.body.name;
    const resourceId = req.body.resourceId;
    const permissionId = req.body.permissionId;
    const desc = req.body.desc;

    try {
      // Does resource-permission exist
      const rp = await ResourcePermission.findOne({
        where: { resourceId, permissionId },
      });
      if (rp) {
        res.status(400).json({ message: 'Resource already exist' });
      } else {
        // If user does not exist
        let newRP = await ResourcePermission.create({
          id,
          name,
          resourceId,
          permissionId,
          desc,
        });
        res.json(newRP);
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

export default router;
