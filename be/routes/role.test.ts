import request from 'supertest';
import app from '../test/setup';

let mockRoleTest: string;

const mockRole = {
  id: 'someId',
  role_name: 'someRoleName',
};

const mockRoles = [
  {
    id: 'someId',
    role_name: 'someRoleName',
  },
  {
    id: 'someId',
    role_name: 'someRoleName',
  },
];

jest.mock('../models/role', () => {
  return {
    findAll: jest.fn().mockImplementation(() => {
      switch (mockRoleTest) {
        case 'success':
          return mockRoles;
        case 'failure':
          return [];
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),

    findOne: jest.fn().mockImplementation(() => {
      switch (mockRoleTest) {
        case 'success':
          return mockRole;
        case 'failure':
          return null;
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),

    create: jest.fn().mockImplementation(() => {
      return mockRole;
    }),
  };
});

describe('GET /', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should retrieve all roles from db', async (done) => {
    mockRoleTest = 'success';
    const res = await request(app).get('/api/role');
    expect(res.body).toEqual(mockRoles);
    expect(res.status).toBe(200);
    done();
  });

  it('should return 404 if no roles found', async (done) => {
    mockRoleTest = 'failure';
    const res = await request(app).get('/api/role');
    expect(res.body.message).toBe('No Roles found');
    expect(res.status).toBe(404);
    done();
  });

  it('should return 500 if there is an error with query', async (done) => {
    mockRoleTest = 'error';
    const res = await request(app).get('/api/role');
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.status).toBe(500);
    done();
  });
});

// Both id and role_name have the same test
describe('GET /search?id=id', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should return role if role is found', async (done) => {
    mockRoleTest = 'success';
    const res = await request(app).get(`/api/role/search?id=12345`);
    expect(res.body).toEqual(mockRole);
    expect(res.status).toBe(200);
    done();
  });

  it('should return 404 if role is not found given an id', async (done) => {
    mockRoleTest = 'failure';
    const res = await request(app).get(`/api/role/search?id=12345`);
    expect(res.body.message).toBe('No Roles found');
    expect(res.status).toBe(404);
    done();
  });

  it('should return 404 if no query is provided', async (done) => {
    mockRoleTest = 'failure';
    const res = await request(app).get(`/api/role/search`);
    expect(res.body.message).toBe('No Roles found');
    expect(res.status).toBe(404);
    done();
  });

  it('should return 500 if there is an error with query', async (done) => {
    mockRoleTest = 'error';
    const res = await request(app).get(`/api/role/search?id=12345`);
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.status).toBe(500);
    done();
  });
});

describe('GET /create', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should return role if successfully created', async (done) => {
    mockRoleTest = 'failure';
    const res = await request(app).post(`/api/role/create`).send(mockRole);
    expect(res.body).toEqual(mockRole);
    expect(res.status).toBe(200);
    done();
  });

  it('should return 400 if role is already exist', async (done) => {
    mockRoleTest = 'success';
    const res = await request(app).post(`/api/role/create`).send(mockRole);
    expect(res.body.message).toBe('Role already exist');
    expect(res.status).toBe(400);
    done();
  });

  it('should return 500 if role is already exist', async (done) => {
    mockRoleTest = 'error';
    const res = await request(app).post(`/api/role/create`).send(mockRole);
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.status).toBe(500);
    done();
  });
});
