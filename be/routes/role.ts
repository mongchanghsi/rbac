import express, { Request, Response } from 'express';
import Role from '../database/models/role';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router({
  strict: true,
});

// @route   GET /
// @desc    Retrive all roles
// @access  Private
router.get(
  '/',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const roles = await Role.findAll({ include: [{ all: true }] });
      if (roles.length > 0) {
        res.json(roles);
      } else {
        res.status(404).json({ message: 'No Roles found' });
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   GET /search?id=id or /search?role_name=role_name
// @desc    Retrive role based on id / role_name
// @access  Private
router.get(
  '/search',
  async (req: Request, res: Response): Promise<void> => {
    const id = req.query.id;
    const role_name = req.query.role_name;
    try {
      let role: any;
      if (id) {
        role = await Role.findOne({
          where: { id },
        });
      } else if (role_name) {
        role = await Role.findOne({
          where: { role_name },
        });
      }

      if (role) {
        res.json(role);
      } else {
        res.status(404).json({ message: 'No Roles found' });
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /create
// @desc    Create a new role
// @access  Private
router.post(
  '/create',
  async (req: Request, res: Response): Promise<void> => {
    const id = uuidv4();
    const role_name = req.body.role_name;

    try {
      // Does role exist
      const role = await Role.findOne({ where: { role_name } });
      if (role) {
        res.status(400).json({ message: 'Role already exist' });
      } else {
        // If role does not exist
        let newRole = await Role.create({ id, role_name });
        res.json(newRole);
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

export default router;
