import request from 'supertest';
import app from '../test/setup';

let mockUserTest: string;
let mockRoleTest: string;

const mockUser = {
  username: 'mockUsername',
  password: 'password',
  role_name: 'mockRole',
};

const mockUsers = [
  {
    id: 'someId',
    username: 'someUser',
    password: 'somePasswrd',
    token: 'someToken',
  },
  {
    id: 'someId',
    username: 'someUser',
    password: 'somePasswrd',
    token: 'someToken',
  },
];

const mockRole = {
  id: 'someId',
  role_name: 'someMockRole',
};

const mockAppointment = {
  id: 'someId',
  userId: 'asd',
  roleId: 'someId',
  projectId: 'SUPER_PROJECT',
};

jest.mock('../models/user', () => {
  return {
    findAll: jest.fn().mockImplementation(() => {
      switch (mockUserTest) {
        case 'success':
          return mockUsers;
        case 'failure':
          return [];
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),

    findOne: jest.fn().mockImplementation(() => {
      switch (mockUserTest) {
        case 'success':
          return mockUser;
        case 'failure':
          return null;
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),

    create: jest.fn().mockImplementation(() => {
      return mockUser;
    }),
  };
});

jest.mock('../models/role', () => {
  return {
    findOne: jest.fn().mockImplementation(() => {
      switch (mockRoleTest) {
        case 'success':
          return mockRole;
        case 'failure':
          return null;
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),
  };
});

jest.mock('../models/appointment', () => {
  return {
    create: jest.fn().mockImplementation(() => {
      return mockAppointment;
    }),
  };
});

describe('GET /', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should retrieve all users from db', async (done) => {
    mockUserTest = 'success';
    const res = await request(app).get('/api/user');
    expect(res.body).toEqual(mockUsers);
    expect(res.status).toBe(200);
    done();
  });

  it('should return 404 if no user found', async (done) => {
    mockUserTest = 'failure';
    const res = await request(app).get('/api/user');
    expect(res.body.message).toBe('No Users found');
    expect(res.status).toBe(404);
    done();
  });

  it('should return 500 if there is an error with query', async (done) => {
    mockUserTest = 'error';
    const res = await request(app).get('/api/user');
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.status).toBe(500);
    done();
  });
});

describe('GET /search', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  let query: string = 'mockUsername';

  it('should return user if the user is found', async (done) => {
    mockUserTest = 'success';
    const res = await request(app).get(`/api/user/search?${query}`);
    expect(res.body).toEqual(mockUser);
    expect(res.status).toBe(200);
    done();
  });
  it('should return user if the user is not found', async (done) => {
    mockUserTest = 'failure';
    const res = await request(app).get(`/api/user/search?${query}`);
    expect(res.body.message).toBe('No User found');
    expect(res.status).toBe(404);
    done();
  });
  it('should return 500 if there is an error with query', async (done) => {
    mockUserTest = 'error';
    const res = await request(app).get(`/api/user/search?${query}`);
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.status).toBe(500);
    done();
  });
});

describe('POST /register', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should return user if user is successfully registered', async (done) => {
    mockUserTest = 'failure'; // this failure meant searching null user => registering new user
    mockRoleTest = 'success';
    const res = await request(app).post(`/api/user/register`).send(mockUser);
    expect(res.body).toEqual(mockUser);
    expect(res.status).toBe(200);
    done();
  });

  it('should return 400 if the user is already exist', async (done) => {
    mockUserTest = 'success'; // this success meant there exist a user
    const res = await request(app).post(`/api/user/register`).send(mockUser);
    expect(res.body.message).toBe('User already exist');
    expect(res.status).toBe(400);
    done();
  });

  it('should return 500 if there is an error with query', async (done) => {
    mockUserTest = 'error';
    const res = await request(app).post(`/api/user/register`).send(mockUser);
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.status).toBe(500);
    done();
  });
});
