import express, { Request, Response } from 'express';
import User from '../database/models/user';
import { checkToken } from '../src/authentication';
import Appointment from '../database/models/appointment';
import Role from '../database/models/role';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router();

interface RequestUser extends Request {
  user?: any;
}

// @route   GET /
// @desc    Retrieve all users
// @access  Public
router.get(
  '/',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const users = await User.findAll({ include: [{ all: true }] });
      if (users.length > 0) {
        res.json(users);
      } else {
        res.status(404).json({ message: 'No Users found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   GET /search
// @desc    Query one user by username
// @access  Private
router.get(
  '/search',
  async (req: Request, res: Response): Promise<void> => {
    try {
      const user = await User.findOne({
        where: { username: req.query.username },
        include: [{ all: true }],
      });
      if (user) {
        res.json(user);
      } else {
        res.status(404).json({ message: 'No User found' });
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   POST /register
// @desc    Register a new user
// @access  Private
router.post(
  '/register',
  async (req: Request, res: Response): Promise<any> => {
    const userId = uuidv4();
    const username = req.body.username;
    const password = req.body.password;

    const appointment_id = uuidv4();
    const role_name = req.body.role_name; // To assign a user a role during registration

    try {
      // Does user exist
      const user = await User.findOne({ where: { username } });
      if (user) {
        res.status(400).json({ message: 'User already exist' });
      } else {
        // If user does not exist
        const newUser = await User.create({ id: userId, username, password });

        // Retrieve the roleId
        const role = await Role.findOne({ where: { role_name } });
        if (!role)
          return res
            .status(400)
            .json({ message: 'Role not found, Bad request' }); // Make sure that front-end is a dropdown list

        // Set a default role for user via appointment into the SUPER_PROJECT
        const newAppointment = await Appointment.create({
          id: appointment_id,
          userId,
          roleId: role.id,
          projectId: 'SUPER_PROJECT',
        });
        res.json(newUser);
      }
    } catch (error) {
      console.log(error.message);
      res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

// @route   GET /retrieveuser
// @desc    Retrieve user by token via middleware
// @access  Public
router.get(
  '/retrieveuser',
  checkToken,
  async (req: RequestUser, res: Response): Promise<void> => {
    if (req.user) {
      res.json(req.user);
    } else {
      res.status(404).json({ message: 'Please sign in again' });
    }
  },
);

export default router;
