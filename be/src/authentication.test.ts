import { checkToken, checkPermissions } from './authentication';

let mockUserTest: string;
let mockAppointmentTest: string;
let mockAccessRightTest: string;
let mockRPTest: string;

jest.mock('../models/user', () => {
  return {
    findOne: jest.fn().mockImplementation(() => {
      switch (mockUserTest) {
        case 'success':
          return {
            username: 'myTestUsername',
            password: 'myTestPassword',
            token: 'myTestToken',
          };
        case 'failure':
          return null;
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),
  };
});

jest.mock('../models/appointment', () => {
  return {
    findAll: jest.fn().mockImplementation(() => {
      switch (mockAppointmentTest) {
        case 'success':
          return [{ roleId: 'someRoleId' }];
        case 'failure':
          return null;
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),
  };
});

jest.mock('../models/accessright', () => {
  return {
    findAll: jest.fn().mockImplementation(() => {
      switch (mockAccessRightTest) {
        case 'success':
          return [{ resourcepermissionId: 'someRPId' }];
        case 'failure':
          return null;
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),
  };
});

jest.mock('../models/resourcepermission', () => {
  return {
    findOne: jest.fn().mockImplementation(() => {
      switch (mockRPTest) {
        case 'success':
          return { id: 'someRPId' };
        case 'failure':
          return { id: 'someAnotherRPId' };
        case 'error':
          throw Error('DB throw error');
        default:
          return false;
      }
    }),
  };
});

const mockUser = {
  id: 'mockUserId',
  username: 'mockUsername',
  password: 'password',
  token: null,
};

describe('checkToken', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  let req: any = {
    cookies: {
      token: 'someToken',
    },
  };
  const res = { json: jest.fn(() => res), status: jest.fn(() => res) };
  const next = jest.fn();

  it('valid user token should return user & and called next', async () => {
    mockUserTest = 'success';
    const user = await checkToken(req, res, next);
    expect(next).toHaveBeenCalledTimes(1);
    expect(req.user).toBeTruthy();
  });

  it('invalid user should return 404', async () => {
    mockUserTest = 'failure';
    const user = await checkToken(req, res, next);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      message: 'No user associated with the token found',
    });
    expect(next).not.toHaveBeenCalled();
  });

  it('throwing error during findOne should return 500', async () => {
    mockUserTest = 'error';
    const user = await checkToken(req, res, next);
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ message: 'Internal Server Error' });
    expect(next).not.toHaveBeenCalled();
  });

  it('invalid token type return 404', async () => {
    req.cookies.token = 123;
    const user = await checkToken(req, res, next);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ message: 'Invalid token' });
    expect(next).not.toHaveBeenCalled();
  });

  it('no token return 404', async () => {
    req.cookies.token = '';
    const user = await checkToken(req, res, next);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ message: 'No token' });
    expect(next).not.toHaveBeenCalled();
  });
});

describe('checkPermissions', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  let req: any = {
    user: mockUser,
    path: '/someAction',
    body: {
      projectId: 'someProjectId',
    },
  };
  const res = { json: jest.fn(() => res), status: jest.fn(() => res) };
  const next = jest.fn();

  it('valid permissions for the role should called next', async () => {
    mockAppointmentTest = 'success';
    mockAccessRightTest = 'success';
    mockRPTest = 'success';
    const valid = await checkPermissions(req, res, next);
    expect(next).toHaveBeenCalled();
  });

  it('invalid permissions for the role return 400', async () => {
    mockRPTest = 'failure';
    const valid = await checkPermissions(req, res, next);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledWith({
      message: 'User is not authorized',
    });
    expect(next).not.toHaveBeenCalled();
  });

  it('no permission found for given role should return 404', async () => {
    mockAccessRightTest = 'failure';
    const valid = await checkPermissions(req, res, next);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      message: 'User has no any access rights in the project',
    });
    expect(next).not.toHaveBeenCalled();
  });

  it('throwing error during any query should return 500', async () => {
    mockAppointmentTest = 'error';
    const valid = await checkPermissions(req, res, next);
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ message: 'Internal Server Error' });
    expect(next).not.toHaveBeenCalled();
  });

  it('missing user should return 400', async () => {
    req.user = undefined;
    const valid = await checkPermissions(req, res, next);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Missing user',
    });
    expect(next).not.toHaveBeenCalled();
  });
});
