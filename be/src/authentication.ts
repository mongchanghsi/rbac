import { Request, Response, NextFunction } from 'express';
import User from '../database/models/user';
import Permission from '../database/models/permission';
import Role from '../database/models/role';
import { Op } from 'sequelize';
import Appointment from '../database/models/appointment';
import AccessRight from '../database/models/accessright';
import ResourcePermission from '../database/models/resourcepermission';

interface RequestUser extends Request {
  user?: any;
}

// Check whether is the session token still valid
export const checkToken = async (
  req: RequestUser,
  res: Response,
  next: NextFunction,
): Promise<any> => {
  console.log('Currently checking token');
  const token = req.cookies.token;
  if (!token) {
    return res.status(400).json({ message: 'No token' });
  }
  if (typeof token !== 'string') {
    return res.status(400).json({ message: 'Invalid token' });
  }

  try {
    // Does token exist within the user table
    const user = await User.findOne({
      where: { token },
    });
    if (user) {
      req.user = user;
      next();
    } else {
      return res
        .status(404)
        .json({ message: 'No user associated with the token found' });
    }
  } catch (error) {
    console.log(error.message);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

// Check whether does the user has the permission to do certain action
export const checkPermissions = async (
  req: RequestUser,
  res: Response,
  next: NextFunction,
): Promise<any> => {
  console.log('Currently checking permission');
  // projectId is taken from the url provided from the front-end
  const projectId = req.body.projectId;

  // Missing user
  if (!req.user) {
    return res.status(400).json({ message: 'Missing user' });
  }

  try {
    let roleId_array: string[] = [];
    let accessrights_array: string[] = [];

    // Retrieve his/her role
    const appointments = await Appointment.findAll({
      where: { userId: req.user.id, projectId },
    });

    if (appointments) {
      // There could be more than 1 role for a user in 1 project
      // Extract out the roleId into any array
      roleId_array = appointments.map((appointment) => appointment.roleId);
    } else {
      return res
        .status(404)
        .json({ message: 'User has no appointment in the project' });
    }
    const accessrights = await AccessRight.findAll({
      where: { roleId: { [Op.or]: [roleId_array] } },
    });

    if (accessrights) {
      // Assuming that the ResourcePermissionId is the action name for the resource
      accessrights_array = accessrights.map(
        (right) => right.resourcepermissionId,
      );
    } else {
      return res
        .status(404)
        .json({ message: 'User has no any access rights in the project' });
    }

    // Assuming that the API endpoint's URL is the name of the action
    const action_path = req.path.slice(1);
    const rp = await ResourcePermission.findOne({
      where: { name: action_path },
    });

    if (!rp) return res.status(404).json({ message: 'Invalid action' });

    if (accessrights_array.includes(rp.id)) {
      next();
    } else {
      return res.status(401).json({ message: 'User is not authorized' });
    }
  } catch (error) {
    console.log(error.message);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};
