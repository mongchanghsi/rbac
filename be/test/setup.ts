import express, { Express } from 'express';

// Import Routes
import UserAPI from '../routes/user';
import RoleAPI from '../routes/role';
import PermissionAPI from '../routes/permission';
import AuthAPI from '../routes/auth';
import ProjectAPI from '../routes/project';
import AppointmentAPI from '../routes/appointment';
import ResourceAPI from '../routes/resource';
import ResourcePermissionAPI from '../routes/resourcepermission';
import AccessRightAPI from '../routes/accessright';

// Express Set up
const app: Express = express();
app.use(express.json());

// API routes
app.use('/api/user', UserAPI);
app.use('/api/role', RoleAPI);
app.use('/api/permission', PermissionAPI);
app.use('/api/auth', AuthAPI);
app.use('/api/project', ProjectAPI);
app.use('/api/appointment', AppointmentAPI);
app.use('/api/resource', ResourceAPI);
app.use('/api/resourcepermission', ResourcePermissionAPI);
app.use('/api/accessright', AccessRightAPI);

export default app;
