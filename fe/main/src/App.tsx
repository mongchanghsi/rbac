import React from 'react';
import './App.css';
import Auth from './components/Auth';
import Dashboard from './components/Dashboard';
import StartProject from './components/Project/StartProject';
import Project from './components/Project';
import { Route, Switch } from 'react-router-dom';

function App() {
  return (
    <div className='App'>
      <Switch>
        <Route path='/' component={Auth} exact />
        <Route path='/dashboard' component={Dashboard} />
        <Route path='/startproject' component={StartProject} />
        <Route path='/project' component={Project} />
      </Switch>
    </div>
  );
}

export default App;
