import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';

interface IForm {
  username: string;
  password: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '30px 0 30px 0',
    },
    card: {
      margin: 'auto',
      width: 400,
    },
    form: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    form_body: {
      margin: 'auto',
      padding: '15px 0 15px 0',
    },
  })
);

const Signup: React.FC = (): React.ReactElement => {
  const history = useHistory();
  const classes = useStyles();

  const [formData, setFormData] = useState<IForm>({
    username: '',
    password: '',
  });

  const updateForm = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setFormData({ ...formData, [e.target.id]: e.target.value });
  };

  const submit = async (e: React.MouseEvent): Promise<void> => {
    e.preventDefault();
    const requestOptions: RequestInit = {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username: formData.username,
        password: formData.password,
      }),
    };
    const res = await fetch(
      'http://localhost:4000/api/auth/login',
      requestOptions
    );
    const parsed = await res.json();
    console.log(parsed); // user object
    history.push(`/dashboard`);
  };

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardContent>
          <Typography id='header' variant='h5' component='h2'>
            Sign In
          </Typography>
          <form className={classes.form} noValidate autoComplete='off'>
            <div className={classes.form_body}>
              <div>
                <TextField
                  id='username'
                  type='text'
                  label='Username'
                  value={formData.username}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
              <div>
                <TextField
                  id='password'
                  type='text'
                  label='Password'
                  value={formData.password}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
            </div>
          </form>
          <Button id='signin' onClick={(e) => submit(e)}>
            Sign In
          </Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default Signup;
