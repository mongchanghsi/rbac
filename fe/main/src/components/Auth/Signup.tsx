import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';

interface IForm {
  id: string;
  username: string;
  password: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '30px 0 30px 0',
    },
    card: {
      margin: 'auto',
      width: 400,
    },
    form: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    form_body: {
      margin: 'auto',
      padding: '15px 0 15px 0',
    },
  })
);

const Signup: React.FC = (): React.ReactElement => {
  const history = useHistory();
  const classes = useStyles();

  const [formData, setFormData] = useState<IForm>({
    id: '',
    username: '',
    password: '',
  });
  // const [error, setError] = useState<string>('');

  const updateForm = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setFormData({ ...formData, [e.target.id]: e.target.value });
  };

  const submit = async (e: React.MouseEvent): Promise<void> => {
    console.log('registered');
  };

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant='h5' component='h2'>
            Sign Up
          </Typography>
          <form className={classes.form} noValidate autoComplete='off'>
            <div className={classes.form_body}>
              <div>
                <TextField
                  id='id'
                  type='text'
                  label='User Id'
                  value={formData.id}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
              <div>
                <TextField
                  id='username'
                  type='text'
                  label='Username'
                  value={formData.username}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
              <div>
                <TextField
                  id='password'
                  type='text'
                  label='Password'
                  value={formData.password}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
            </div>
          </form>
          <Button onClick={(e) => submit(e)}> Register </Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default Signup;
