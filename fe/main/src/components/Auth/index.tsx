import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import Signin from './Signin';
import Signup from './Signup';

const Auth: React.FC = (): React.ReactElement => {
  const [toggle, setToggle] = useState<boolean>(true);

  return (
    <div>
      <Button onClick={() => setToggle((prev) => !prev)}>
        {toggle ? <h4> Go to register page </h4> : <h4> Go to log in page </h4>}
      </Button>
      {toggle ? <Signin /> : <Signup />}
    </div>
  );
};

export default Auth;
