import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Card, CardContent, Button, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import ProjectEntry from './ProjectEntry';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '30px 0 30px 0',
    },
    card: {
      margin: 'auto',
      width: 800,
    },
    form: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    form_body: {
      margin: 'auto',
      padding: '15px 0 15px 0',
    },
  })
);

const Dashboard: React.FC = (): React.ReactElement => {
  const history = useHistory();
  const classes = useStyles();

  const [user, setUser] = useState<any>({});
  const [role, setRole] = useState<string>('');
  const [projects, setProjects] = useState<any>([]);

  // useEffect => taking the token stored in cookie => run through a fetch request to retreive user obj
  // if user obj does not exist => redirect back to home page => else it will remain in the page with the user's name populated
  const requestOptions: RequestInit = {
    method: 'GET',
    mode: 'cors',
    credentials: 'include',
    headers: { 'Content-Type': 'application/json' },
  };

  const startup = async () => {
    try {
      // Retrieve User Object
      const parsedUser = await retrieveUserObj();
      setUser(parsedUser);

      // Retrieve User's Default Role
      const currentUserRole = await retrieveRole(parsedUser.id);
      setRole(currentUserRole);

      // Retrieve User's Project
      // 1. Retrieve all appointments from table w.o SUPER_PROJECT
      // 2. Extract out all the projectIds
      // 3. Loop through list of projectId to get objects of projects
      // 4. Display project title on dashboard
      const projectListFromAppointment = await retrieveProjectList(
        parsedUser.id
      );
      const projectListId: string[] = projectListFromAppointment.map(
        (project: any) => project.projectId
      );
      let projectList = projectListId.map((id: string) =>
        retrieveIndividualProject(id)
      );
      projectList = await Promise.all(projectList);
      setProjects(projectList);
    } catch (error) {
      console.log(error.message);
      history.push('/');
    }
  };

  const retrieveUserObj = async () => {
    try {
      const res = await fetch(
        'http://localhost:4000/api/user/retrieveuser',
        requestOptions
      );
      if (!res.ok) {
        throw Error('Sign in again');
      }
      return await res.json();
    } catch (error) {
      console.log('Error at retrieveUserObj');
    }
  };

  const retrieveRole = async (id: string) => {
    try {
      const appointment = await fetch(
        `http://localhost:4000/api/appointment/retrieve_default_role/${id}`,
        requestOptions
      );
      const parsedAppointment = await appointment.json();
      const res = await fetch(
        `http://localhost:4000/api/role/search?id=${parsedAppointment.roleId}`,
        requestOptions
      );
      const parsedRes = await res.json();
      return parsedRes.role_name;
    } catch (error) {
      console.log('Error at RetrieveRole');
    }
  };

  const retrieveProjectList = async (id: string) => {
    try {
      const appointments = await fetch(
        `http://localhost:4000/api/appointment/${id}`,
        requestOptions
      );
      const parsedAppointments = await appointments.json();
      return parsedAppointments;
    } catch (error) {
      console.log('Error at retrieveProjectList');
    }
  };

  const retrieveIndividualProject = async (projectId: string) => {
    const projectItem = await fetch(
      `http://localhost:4000/api/project/search?id=${projectId}`,
      requestOptions
    );
    return await projectItem.json();
  };

  useEffect(() => {
    startup();
  }, []);

  const logout = () => {
    history.push('/');
  };

  const goToStartProject = () => {
    history.push('/startproject');
  };

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant='h5' component='h2'>
            User: {user.username}
          </Typography>
          <p>Current Role: {role}</p>
          <Button onClick={goToStartProject}> Create a new project </Button>
          <Button onClick={logout}> Log out </Button>
        </CardContent>
      </Card>

      <Card className={classes.card}>
        <CardContent>
          <Typography variant='h5' component='h2'>
            Projects
          </Typography>
          {projects.length > 0 ? (
            <div>
              {projects.map((project: any) => (
                <div key={project.id}>
                  <ProjectEntry project={project} />
                </div>
              ))}
            </div>
          ) : (
            <h5> No projects available </h5>
          )}
        </CardContent>
      </Card>
    </div>
  );
};

export default Dashboard;
