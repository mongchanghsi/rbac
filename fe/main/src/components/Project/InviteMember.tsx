import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { v4 as uuidv4 } from 'uuid';

type IProps = {
  children?: React.ReactNode;
  project: any;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '30px 0 30px 0',
    },
    card: {
      margin: 'auto',
      width: 400,
    },
    form: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    form_body: {
      margin: 'auto',
      padding: '15px 0 15px 0',
    },
  })
);

const InviteMember: React.FC<IProps> = (props: IProps): React.ReactElement => {
  const classes = useStyles();

  const [formData, setFormData] = useState<any>({
    user: '',
    role: '',
  });
  const [project] = useState<any>(props.project);

  const updateForm = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setFormData({ ...formData, [e.target.id]: e.target.value });
  };

  const invite = async (e: React.MouseEvent): Promise<void> => {
    e.preventDefault();
    // Look up the roles for roleId using role_name
    const requestOptions_GET: RequestInit = {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
      headers: { 'Content-Type': 'application/json' },
    };
    const role = await fetch(
      `http://localhost:4000/api/role/search?role_name=${formData.role}`,
      requestOptions_GET
    );
    const parsedRole = await role.json();

    // Look up user for userId using username
    const user = await fetch(
      `http://localhost:4000/api/user/search?username=${formData.user}`,
      requestOptions_GET
    );
    const parsedUser = await user.json();

    const requestOptions_POST: RequestInit = {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        id: uuidv4(),
        projectId: project.id,
        userId: parsedUser.id,
        roleId: parsedRole.id,
      }),
    };
    const res = await fetch(
      'http://localhost:4000/api/appointment/access_create',
      requestOptions_POST
    );
    const parsedRes = await res.json();
    console.log(parsedRes);
  };

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardContent>
          <Typography id='editrole' variant='h5' component='h5'>
            Invite Member
          </Typography>
          <form className={classes.form} noValidate autoComplete='off'>
            <div className={classes.form_body}>
              <div>
                <TextField
                  id='user'
                  type='text'
                  label='User'
                  value={formData.user}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
              <div>
                <TextField
                  id='role'
                  type='text'
                  label='Role'
                  value={formData.password}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
            </div>
          </form>
          <Button id='invite' onClick={(e) => invite(e)}>
            Invite
          </Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default InviteMember;
