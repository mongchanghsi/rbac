import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';

interface IForm {
  title: string;
  desc: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '30px 0 30px 0',
    },
    card: {
      margin: 'auto',
      width: 800,
    },
    form: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    form_body: {
      margin: 'auto',
      padding: '15px 0 15px 0',
    },
  })
);

const StartProject: React.FC = (): React.ReactElement => {
  const history = useHistory();
  const classes = useStyles();

  const [formData, setFormData] = useState<IForm>({
    title: '',
    desc: '',
  });

  const updateForm = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setFormData({ ...formData, [e.target.id]: e.target.value });
  };

  const requestOptions: RequestInit = {
    method: 'POST',
    mode: 'cors',
    credentials: 'include',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      title: formData.title,
      desc: formData.desc,
    }),
  };

  const create = async () => {
    console.log('Creating Project');
    const resProject = await fetch(
      `http://localhost:4000/api/project/project_create`,
      requestOptions
    );
    const parsedProject = await resProject.json();
    console.log(parsedProject);
    history.push('/dashboard');
  };

  const back = () => {
    history.push('/dashboard');
  };

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant='h5' component='h2'>
            Start a new project
          </Typography>
          <form className={classes.form} noValidate autoComplete='off'>
            <div className={classes.form_body}>
              <div>
                <TextField
                  id='title'
                  type='text'
                  label='title'
                  value={formData.title}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
              <div>
                <TextField
                  id='desc'
                  type='text'
                  label='desc'
                  value={formData.desc}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    updateForm(e)
                  }
                  required
                />
              </div>
            </div>
          </form>
          <Button onClick={create}> Start Project </Button>
          <Button onClick={back}>Back to Dashboard</Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default StartProject;
