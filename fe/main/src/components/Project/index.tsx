import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  Button,
  Typography,
  TextField,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import EditRole from './EditRole';
import InviteMember from './InviteMember';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '30px 0 30px 0',
    },
    card: {
      margin: 'auto',
      width: 800,
    },
    form: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    form_body: {
      margin: 'auto',
      padding: '15px 0 15px 0',
    },
  })
);

const Project: React.FC = (): React.ReactElement => {
  const history = useHistory();
  const classes = useStyles();

  const [project, setProject] = useState<any>({});
  const [editAccess, setEditAccess] = useState<boolean>(false);
  const [role, setRole] = useState<string>('');

  const back = () => {
    history.push('/dashboard');
  };

  const requestOptions: RequestInit = {
    method: 'GET',
    mode: 'cors',
    credentials: 'include',
    headers: { 'Content-Type': 'application/json' },
  };

  const retrieveProject = async () => {
    const projectId = document.URL.slice(30);
    const res = await fetch(
      `http://localhost:4000/api/project/search?id=${projectId}`,
      requestOptions
    );
    setProject(await res.json());
  };

  const retrieveUserRoleProject = async () => {
    // What is the user's role in this project?
    const projectId = document.URL.slice(30);
    const appointment = await fetch(
      `http://localhost:4000/api/appointment/retrieve_role/${projectId}`,
      requestOptions
    );
    const parsedAppointment = await appointment.json();
    const res = await fetch(
      `http://localhost:4000/api/role/search?id=${parsedAppointment.roleId}`,
      requestOptions
    );
    const parsedRes = await res.json();
    setRole(parsedRes.role_name);
  };

  useEffect(() => {
    retrieveProject();
    retrieveUserRoleProject();
  }, []);

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant='h5' component='h2'>
            Project: {project.title}
          </Typography>
          <p>ID: {project.id}</p>
          <p>Role: {role}</p>
          <p>Description: {project.desc}</p>
          {editAccess && (
            <>
              <InviteMember project={project} />
              <EditRole project={project} />
            </>
          )}
          <Button onClick={() => setEditAccess(!editAccess)}>
            {editAccess ? <> Back to Project </> : <> Edit Access </>}
          </Button>
          <Button onClick={back}>Back to Dashboard</Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default Project;
