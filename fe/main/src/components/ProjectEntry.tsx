import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Card, CardContent, Button, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

type IProps = {
  children?: React.ReactNode;
  project: any;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '30px 0 30px 0',
    },
    card: {
      margin: 'auto',
      width: 800,
    },
    form: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    form_body: {
      margin: 'auto',
      padding: '15px 0 15px 0',
    },
  })
);

const ProjectEntry: React.FC<IProps> = (props: IProps): React.ReactElement => {
  const history = useHistory();
  const classes = useStyles();

  const project = props.project;
  const [name] = useState<any>(project.title);

  const view = () => {
    history.push(`/project?${project.id}`);
  };
  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardContent>
          <p>Project Name: {name}</p>
          <Button onClick={view}> View more </Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default ProjectEntry;
