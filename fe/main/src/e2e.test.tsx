import React from 'react';
import selenium from 'selenium-webdriver';

const By = selenium.By;
let driver: any;

beforeEach(async () => {
  driver = new selenium.Builder().forBrowser('chrome').build();
  await driver.get('http://localhost:3000/');
});

afterEach(async () => {
  await driver.quit();
});

test('E2E Test 1 | User is able to log in and log out - Logs In & Logs out', async () => {
  const usernameField = await driver.findElement(By.id('username'));
  const passwordField = await driver.findElement(By.id('password'));

  usernameField.sendKeys('username1');
  passwordField.sendKeys('password');

  const signin = await driver.findElement(By.id('signin'));
  await signin.click();

  await driver.manage().setTimeouts({ implicit: 3000 });

  const logout = await driver.findElement(By.id('logout'));
  await logout.click();

  const header = await driver.findElement(By.id('header')).getText();
  expect(header).toBe('Sign In');
});

test('E2E Test 2 | User is authorized for an action - Logs In & click on Action All Button should display the result', async () => {
  const usernameField = await driver.findElement(By.id('username'));
  const passwordField = await driver.findElement(By.id('password'));

  usernameField.sendKeys('username1');
  passwordField.sendKeys('password');

  const signin = await driver.findElement(By.id('signin'));
  await signin.click();

  await driver.manage().setTimeouts({ implicit: 3000 });

  const actionAll = await driver.findElement(By.id('actionAll'));
  await actionAll.click();

  const title = await driver.findElement(By.id('result')).getText();
  expect(title).toBe('Action All is successful');
});

test('E2E Test 3 | User is not authorized for an action - Logs In & click on Action 2 Button should display the result', async () => {
  const usernameField = await driver.findElement(By.id('username'));
  const passwordField = await driver.findElement(By.id('password'));

  usernameField.sendKeys('username1');
  passwordField.sendKeys('password');

  const signin = await driver.findElement(By.id('signin'));
  await signin.click();

  await driver.manage().setTimeouts({ implicit: 3000 });

  const action2 = await driver.findElement(By.id('action2'));
  await action2.click();

  const title = await driver.findElement(By.id('result')).getText();
  expect(title).toBe('Action 2 is not successful');
});

test('E2E Test 4 | Admin user is authorize to add role - Logs In & click on edit role & add a new role to user', async () => {
  const usernameField = await driver.findElement(By.id('username'));
  const passwordField = await driver.findElement(By.id('password'));

  usernameField.sendKeys('username1');
  passwordField.sendKeys('password');

  const signin = await driver.findElement(By.id('signin'));
  await signin.click();

  await driver.manage().setTimeouts({ implicit: 3000 });

  const actionEdit = await driver.findElement(By.id('edit'));
  await actionEdit.click();

  const title = await driver.findElement(By.id('editrole')).getText();
  expect(title).toBe('Edit Role');

  await driver.manage().setTimeouts({ implicit: 3000 });

  const userField = await driver.findElement(By.id('user'));
  const roleField = await driver.findElement(By.id('role'));

  userField.sendKeys('user5');
  roleField.sendKeys('role2');

  const add = await driver.findElement(By.id('add'));
  await add.click();
});

test('E2E Test 5 | Admin user is authorize to remove role - Logs In & click on edit role & remove a exisiting role to user', async () => {
  const usernameField = await driver.findElement(By.id('username'));
  const passwordField = await driver.findElement(By.id('password'));

  usernameField.sendKeys('username1');
  passwordField.sendKeys('password');

  const signin = await driver.findElement(By.id('signin'));
  await signin.click();

  await driver.manage().setTimeouts({ implicit: 3000 });

  const actionEdit = await driver.findElement(By.id('edit'));
  await actionEdit.click();

  const title = await driver.findElement(By.id('editrole')).getText();
  expect(title).toBe('Edit Role');

  await driver.manage().setTimeouts({ implicit: 3000 });

  const userField = await driver.findElement(By.id('user'));
  const roleField = await driver.findElement(By.id('role'));

  userField.sendKeys('user5');
  roleField.sendKeys('role2');

  const remove = await driver.findElement(By.id('remove'));
  await remove.click();
});

test('E2E Test 6 | User is not authorize for admin roles - Logs In & click on edit role & show result', async () => {
  const usernameField = await driver.findElement(By.id('username'));
  const passwordField = await driver.findElement(By.id('password'));

  usernameField.sendKeys('username2');
  passwordField.sendKeys('password');

  const signin = await driver.findElement(By.id('signin'));
  await signin.click();

  await driver.manage().setTimeouts({ implicit: 3000 });

  const actionEdit = await driver.findElement(By.id('edit'));
  await actionEdit.click();

  const title = await driver.findElement(By.id('result')).getText();
  expect(title).toBe('You are not authorized to edit roles / permissions');
});
